import tensorflow as tf
from tensorflow.keras.applications import MobileNet
from tensorflow.keras.preprocessing import image
from tensorflow.keras.applications.mobilenet import preprocess_input
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.models import Model
from tensorflow.keras.callbacks import ModelCheckpoint
from tensorflow.keras.optimizers import SGD
from auxilaries import *
from model import *


# Assigned which pretrained model architecture and weights. 
base_model = MobileNet(weights = 'imagenet', include_top=False)
print('MobileNet currently has ' + str(len(base_model.layers)) + ' layers.')

if transfer == True:
    # Append the pre-trained component with additional dense layers
    model = append_model(base_model)
    print('The model now has ' + str(len(model.layers)) + 'layers.')
    filepath = fine_tuned_weight_path

    # Freeze part of the model that will not be updated.
    freeze_layers(model, LAYER_INDEX)
else:
    model = basic_model(INPUT_SHAPE)
    filepath = normal_weight_path


model.summary()

# Since a lot of the pre-trained models on Keras have been ported from different frameworks
# we incorporate their pre-processing method. This is useing the preprocess_input parameter that 
# we import from keras.applications.mobilenet.
# To find more documentation, you will have to read their research paper on what they have done. 
datagen = ImageDataGenerator(preprocessing_function=preprocess_input)
train_generator = datagen.flow_from_directory(train_directory, 
	target_size=TARGET_SIZE, color_mode='rgb', batch_size=BATCH_SIZE, class_mode='categorical', shuffle=True)

val_generator = datagen.flow_from_directory(validation_directory, 
	target_size=TARGET_SIZE, color_mode='rgb', batch_size=BATCH_SIZE, class_mode='categorical', shuffle=True)

model.compile(optimizer='sgd', loss='categorical_crossentropy', metrics=['accuracy'])

checkpoint = ModelCheckpoint(filepath, monitor='val_accuracy', verbose=1, save_best_only=True, mode='max')

step_size_train = train_generator.n // train_generator.batch_size
step_size_val = val_generator.n // val_generator.batch_size

model.fit_generator(generator=train_generator,
                   steps_per_epoch=step_size_train,
                   epochs=EPOCHS, validation_data=val_generator,
                   validation_steps = step_size_val,
                   shuffle=True, callbacks=[checkpoint])

classes = list(train_generator.class_indices.keys())
