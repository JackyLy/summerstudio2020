import gym
from time import sleep
from dqn import DQN
import os

def main():
    env = gym.make("MountainCar-v0")
    dqn_agent = DQN(env=env)
    dqn_agent.load_model('solved.h5')
    dqn_agent.epsilon = 0
    trial_len = 100
    state_size = env.observation_space.shape[0]

    cur_state = env.reset().reshape(1, state_size)
    total_reward = 0

    for trial in range(trial_len):
        action = dqn_agent.act(cur_state)
        new_state, reward, done, _ = env.step(action)
        new_state = new_state.reshape(1,state_size)
        total_reward += reward
        cur_state = new_state

        sleep(0.01)
        env.render()
        if done:
            break

    env.close()
    print('Total Reward for Episode = ', total_reward)


if __name__== '__main__':
    main()
